//
//  CountryListModel.swift
//  CountryListDemo
//
//  Created by Arohi on 12/09/21.
//  Copyright © 2021 Arohi. All rights reserved.
//

import Foundation


import Foundation

// MARK: - CountryListModel
struct CountryListModel: Codable {
    var data: [CountryListData]?
}

// MARK: - CountryListData
struct CountryListData: Codable {
    var country_id, country_code, country_name, status: String?

}







