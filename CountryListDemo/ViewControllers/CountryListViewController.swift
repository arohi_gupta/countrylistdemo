//
//  CountryListViewController.swift
//  CountryListDemo
//
//  Created by Arohi on 12/09/21.
//  Copyright © 2021 Arohi. All rights reserved.
//

import UIKit

class CountryListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK:- Outlets
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:- Variables & Constants
    var countryList: CountryListModel?
    var searchList: CountryListModel = CountryListModel()
    var searchActive = false
    
    //MARK:- ViewDidLoad Method
    override func viewDidLoad() {
        super.viewDidLoad()
        self.countryListData()
        self.tableView.reloadData()
    }
    
    //MARK:- Fetching Json Data
    func countryListData() {
        guard let fileLocation = Bundle.main.url(forResource: "countryList", withExtension: "json") else {
            return
        }
        do {
            let fetchList = try Data(contentsOf: fileLocation)
            countryList = try JSONDecoder().decode(CountryListModel.self, from: fetchList)
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        } catch {
            print("Error: \(error)")
        }
    }
    
    //MARK:- TableView DataSource Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.searchActive == true {
            if self.searchList.data?.count ?? 0 > 0 {
                return 1
            }
        } else {
            if self.countryList?.data?.count ?? 0 > 0 {
                return 1
            }
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.searchActive == true {
            return self.searchList.data?.count ?? 0
        } else {
            return self.countryList?.data?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CountryListCell", for: indexPath) as! CountryListTableViewCell
        
        var getSingleListInfo: CountryListData?
        if self.searchActive == true {
            if let singeItem = self.searchList.data?[indexPath.row]  {
                getSingleListInfo = singeItem
            }
        } else {
            if let singeItem = self.countryList?.data?[indexPath.row]  {
                getSingleListInfo = singeItem
            }
        }
        if let getList = getSingleListInfo  {
            if let countryName = getList.country_name {
                cell.lblCountryName.text = "Country Name : \(countryName)"
            }
            if let countryCode = getList.country_code {
                cell.lblCountryCode.text = "Country Code : \(countryCode)"
            }
            if let countryId = getList.country_id {
                cell.lblCountryId.text = "Country Id : \(countryId)"
            }
            
        }
        return cell
    }
}
