//
//  CountryListTableViewCell.swift
//  CountryListDemo
//
//  Created by Arohi on 12/09/21.
//  Copyright © 2021 Arohi. All rights reserved.
//

import UIKit

class CountryListTableViewCell: UITableViewCell {

    //MARK:- Outlets
    @IBOutlet weak var lblCountryName: UILabel!
    @IBOutlet weak var lblCountryCode: UILabel!
    @IBOutlet weak var lblCountryId: UILabel!
    
    //MARK:- AwakeFromNib Method
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    //MARK:- SetSelected Method
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
