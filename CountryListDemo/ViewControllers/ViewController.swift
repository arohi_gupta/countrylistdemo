//
//  ViewController.swift
//  CountryListDemo
//
//  Created by Arohi on 12/09/21.
//  Copyright © 2021 Arohi. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    //MARK:- Outlet
    @IBOutlet weak var btnCountryList: UIButton!
    
    //MARK:- ViewDidLoad Method
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

 //MARK:- Country List Button Action Method
    @IBAction func btnActionCountryList(_ sender: Any) {
        self.performSegue(withIdentifier: "CountryLIstSegue", sender: nil)
    }
    
}
