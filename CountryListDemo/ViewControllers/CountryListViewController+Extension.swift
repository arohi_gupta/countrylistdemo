//
//  CountryListViewController+Extension.swift
//  CountryListDemo
//
//  Created by Arohi on 12/09/21.
//  Copyright © 2021 Arohi. All rights reserved.
//

import Foundation

//MARK:- Extension For UISearchBarDelegate
extension CountryListViewController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.searchBar.showsCancelButton = true
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchActive = false
        self.searchList.data?.removeAll()
        self.searchBar.text = ""
        self.searchBar.showsCancelButton = false
        self.tableView.reloadData()
        self.searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        self.searchActive = true
        var updatedText = (self.searchBar.text ?? "").trimmingCharacters(in: .whitespacesAndNewlines)
        if updatedText.isEmpty == true {
            updatedText = ""
        }
        self.searchListMethod(updatedText: updatedText)
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
    
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        let currentText = searchBar.text ?? ""
        
        guard let stringRange = Range(range, in: currentText) else { return false }
        
        let updatedText = currentText.replacingCharacters(in: stringRange, with: text)
        
        print(updatedText)
        self.searchListMethod(updatedText: updatedText)
        return true
    }
    
    func searchListMethod(updatedText: String) {
        let updatedText = updatedText.trimmingCharacters(in: .whitespacesAndNewlines)
        if updatedText == "" {
            self.searchActive = false
            self.tableView.reloadData()
            return
        }
        self.searchActive = true
    
        self.searchList.data = self.countryList?.data?.filter({$0.country_name?.range(of: updatedText, options: .caseInsensitive) != nil || $0.country_id?.range(of: updatedText, options: .caseInsensitive) != nil || $0.country_code?.range(of: updatedText, options: .caseInsensitive) != nil})
    
        self.tableView.reloadData()
    }
    
}
